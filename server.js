// Get dependencies

const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require("mongodb");
const ObjectID = mongodb.ObjectID;

const USERS_COLLECTION = "users";
const ITEM_COLLECTION = "items";
const ORDERS_COLLECTION = "orders";
const BASKETS_COLLECTION = "baskets";
const TOKENS_COLLECTION = "tokens";

// Get our API routes
const api = require('./server/routes/api');

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Create link to Angular build directory
let distDir = __dirname + "/dist/";
app.use(express.static(distDir));


// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
let db;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(process.env.MONGODB_URI, function (err, database) {
  if (err) {
    console.log(err);
    process.exit(1);
  }
  // Save database object from the callback for reuse.
  db = database;
  console.log("Database connection ready");
  // Initialize the app.
  let server = app.listen(process.env.PORT || 8080, function () {
    let port = server.address().port;
    console.log("App now running on port", port);
  });
});

// CONTACTS API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

app.post("/api/getUser", function(req, res) {
  let id = req.body.identifier;
  if (!id){
    handleError(res,"Invalid username input","Must provide user id",400);
  }else {
  db.collection(USERS_COLLECTION).findOne({_id: ObjectID(id) }, function (err, doc) {
    if(err){
      handleError(res,err.message,"Failed to load users",300);
    } else {
      console.log("USER IS:");
      console.log(doc);
      res.status(200).json(doc);
    }
  });
  }
});

app.post("/api/loginUser", function (req, res) {
  let checkUser = req.body;
  db.collection(USERS_COLLECTION).findOne({username: checkUser.username, password: checkUser.password},function (err, doc) {
    if (err){
      handleError(res,err.message,"Failed to find username");
    } else {
      let userId = doc._id;
      let token = {_id: doc._id, key: generateToken()};
      db.collection(TOKENS_COLLECTION).updateOne({_id: userId}, token, {upsert: true}, function (err, doc) {
        if(err) {
          handleError(res, err.message,"Failed to update token");
        } else {
          db.collection(TOKENS_COLLECTION).findOne({_id: userId}, function (err, doc) {
            if (err) {
              handleError(res, err.message, "Failed to find token");
            } else {
              res.status(201).json(doc);
            }
          });
        }
      });
    }
  })
});

function generateToken() {
  return "tokentokentoken";
}

app.post("/api/authToken", function (req, res) {
  let token = req.body;
  db.collection(TOKENS_COLLECTION).findOne({_id: token._id, token: token.key }, function (err, doc) {
    if(err) {
      handleError(res, err.message);
    } else {
      res.status(201).json(JSON.stringify({validation: true}));
    }
  });
});

app.post("/api/addUser", function(req, res) {
  let newUser = req.body;
  if (!req.body.username) {
    handleError(res, "Invalid username input", "Must provide a name.", 400);
  }
  db.collection(USERS_COLLECTION).insertOne(newUser, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to create new username.");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});

app.post("/api/addItem", function (req, res) {
  let newItem = req.body;
  db.collection(ITEM_COLLECTION).insertOne(newItem, function (err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to add item");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  })
});

app.get("/api/getItems", function (req, res) {
  db.collection(ITEM_COLLECTION).find().toArray(function (err, doc) {
    if (err) {
      handleError(res, err.message);
    } else {
      res.status(201).json(doc);
    }
  });
});

app.post("/api/addOrder", function (req, res) {
  let order = req.body;
  db.collection(ORDERS_COLLECTION).insertOne(order, function (err, doc) {
    if (err) {
      handleError(res, err.message,"Can't add order");
    } else {
      res.status(201).json(doc);
    }
  })
});

app.post("/api/saveBasket", function (req, res) {
  let savedBasket = req.body;
  db.collection(BASKETS_COLLECTION).updateOne({username: savedBasket.username}, savedBasket, {upsert: true}, function (err, doc) {
    if(err) {
      handleError(res, err.message);
    } else {
      res.status(201).json(doc);
    }
  });
});

app.post("/api/loadBasket", function (req, res) {
  let username = req.body.identifier;
  db.collection(BASKETS_COLLECTION).findOne({username: username}, function (err, doc) {
    if (err) {
      handleError(res, err.message);
    } else {
      res.status(201).json(doc);
    }
  });
});

app.get("/api/getOrders", function (req, res) {
  db.collection(ORDERS_COLLECTION).find().toArray(function (err, doc) {
    if (err) {
      handleError(res, err.message, "Can't load orders")
    } else  {
      res.status(200).json(doc);
    }
  })
});

app.post("/api/userOrders", function (req, res) {
  const username = req.body.identifier;
  db.collection(ORDERS_COLLECTION).find({username: username}).toArray(function (err, docs) {
    if (err) {
      handleError(res, err.message, "Can't load user orders");
    } else {
      res.status(200).json(docs);
    }
  })
});

app.post("/api/updateOrder", function (req, res) {
  const order = req.body;
  const recreateOrder = {orderItems: order.orderItems, orderInfo: order.orderInfo,
    username: order.username, orderStatus: parseInt(order.orderStatus), _id: ObjectID(order._id)};
  db.collection(ORDERS_COLLECTION).updateOne({_id: ObjectID(order._id)}, recreateOrder, function (err, doc) {
    if (err) {
      handleError(res, err.message, "Can't update order")
    } else  {
      res.status(200).json(doc);
    }
  })
});

