import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {ItemDetailComponent} from './components/item-list-n-detail/app.item-detail.component';
import {ItemListComponent} from './components/item-list-n-detail/app.item-list.component';
import {CategoryItemComponent} from './components/category-list-n-item/app.category-item.component';
import {CategoryListComponent} from './components/category-list-n-item/app.category-list.component';
import {ItemProviderService} from './services/item-provider.service/item-provider.service';
import {BasketService} from './services/basket.service/basket.service';
import { TopBasketComponent } from './components/top-basket/top-basket.component';
import { BasketComponent } from './components/basket/basket.component';
import { BasketItemComponent } from './components/basket-item/basket-item.component';
import {StateService} from './services/stateService/state.service';
import { OrderPlacementComponent } from './components/order-placement/order-placement.component';
import {ReactiveFormsModule} from '@angular/forms';
import { FieldErrorDisplayComponent } from './components/field-error-display/field-error-display.component';
import {OrderService} from './services/order.service/order.service';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { UserRegisterComponent } from './components/user-register/user-register.component';
import {HttpClientModule} from '@angular/common/http';
import { WorkspaceComponent } from './components/workspace/workspace.component';
import {UserDataService} from './services/user-data.service/user-data.service';
import {HttpModule} from '@angular/http';
import { AddItemComponent } from './components/add-item/add-item.component';
import {ItemDataService} from './services/item-data.service/item-data.service';
import {OrderDataService} from './services/order-data.service/order-data.service';
import {BasketDataService} from './services/basket-data/basket-data.service';
import {UserService} from './services/user.service/user.service';
import { ManageOrdersComponent } from './components/manage-orders/manage-orders.component';
import { OrderDetailComponent } from './components/order-detail/order-detail.component';
import { YourOrdersComponent } from './components/your-orders/your-orders.component';
import { YourOrderDatailComponent } from './components/your-order-datail/your-order-datail.component';



@NgModule({
  declarations: [
    AppComponent,
    ItemDetailComponent,
    ItemListComponent,
    CategoryListComponent,
    CategoryItemComponent,
    TopBasketComponent,
    BasketComponent,
    BasketItemComponent,
    OrderPlacementComponent,
    FieldErrorDisplayComponent,
    UserLoginComponent,
    UserRegisterComponent,
    WorkspaceComponent,
    AddItemComponent,
    ManageOrdersComponent,
    OrderDetailComponent,
    YourOrdersComponent,
    YourOrderDatailComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ItemProviderService,
    BasketService,
    StateService,
    OrderService,
    UserDataService,
    ItemDataService,
    OrderDataService,
    BasketDataService,
    UserService
    ],
  bootstrap: [AppComponent,
    ItemListComponent,
    CategoryItemComponent,
    CategoryListComponent,
    TopBasketComponent]
})
export class AppModule {
}
