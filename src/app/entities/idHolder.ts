export class IdHolder {
  identifier: string;
  constructor(identifier: string) {
    this.identifier = identifier;
  }
}
