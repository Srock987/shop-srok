import {OrderItem} from './orderItem';

export class SavedBasket {
  orderItems: OrderItem[];
  username: string;
  constructor(orderItems: OrderItem[], username: string) {
    this.orderItems = orderItems;
    this.username = username;
  }
}
