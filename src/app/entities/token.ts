export class Token {
  _id: string;
  key: string;
  constructor(_id: string, key: string) {
    this._id = _id;
    this.key = key;
  }
}
