export class User {
  _id: string;
  username: string;
  password: string;
  privilege: Privilege;

  constructor(username: string, password: string, privilege: Privilege = Privilege.NORMAL) {
    this.username = username;
    this.password = password;
    this.privilege = privilege;
  }
}

export enum Privilege {
  NORMAL,
  ADMIN
}
