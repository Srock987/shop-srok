import {OrderItem} from './orderItem';
import {OrderInfo} from './orderInfo';

const USER_UNSPECIFIED = 'user_unspecified';

export class Order {
  _id: string;
  orderItems: OrderItem[];
  orderInfo: OrderInfo;
  username: string;
  orderStatus: OrderStatus;

  constructor(orderItems: OrderItem[], orderInfo: OrderInfo,
              username: string = USER_UNSPECIFIED, orderStatus: OrderStatus = OrderStatus.PLACED, _id?: string  ) {
    this.orderItems = orderItems;
    this.orderInfo = orderInfo;
    this.username = username;
    this.orderStatus = orderStatus;
    this._id = _id;
  }
}

export enum OrderStatus {
  PLACED,
  FINISHED
}

export function fullPrice(order: Order): number {
  let price = 0;
  for (const orderItem of order.orderItems) {
    price += orderItem.quantity * orderItem.item.price;
  }
  return price;
}

export function realStatus(orderStatus: OrderStatus): string {
  switch (orderStatus) {
    case OrderStatus.PLACED: {
      return 'Placed';
    }
    case OrderStatus.FINISHED: {
      return 'Finished';
    }
    default: {
      return 'Unknown';
    }
  }
}
