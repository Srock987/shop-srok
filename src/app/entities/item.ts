export class Item {
  _id: string;
  name: string;
  description: string;
  price: number;
  category: string;
  picture: string;
}
