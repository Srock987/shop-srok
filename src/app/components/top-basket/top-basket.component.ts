import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {BasketService} from '../../services/basket.service/basket.service';
import {Observable} from 'rxjs/Observable';
import {State, StateService} from '../../services/stateService/state.service';

@Component({
  selector: 'app-top-basket',
  templateUrl: './top-basket.component.html',
  styleUrls: ['./top-basket.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TopBasketComponent implements OnInit {
  orderedItemCount: Observable<number>;
  sumPrice: Observable<number>;
  constructor(private basketService: BasketService, private stateService: StateService) { }

  ngOnInit() {
    this.orderedItemCount = this.basketService.numberOfItems;
    this.sumPrice = this.basketService.summaryPrice;
  }
  checkOut() {
    this.stateService.changeState(State.Basket);
  }

}
