import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {fullPrice, Order, OrderStatus, realStatus} from '../../entities/order';
import {OrderService} from '../../services/order.service/order.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class OrderDetailComponent implements AfterViewInit {

  @ViewChild('statusSelect') statusSelect: ElementRef;
  @Input() order: Order;
  stateType: any = OrderStatus;
  constructor(private orderService: OrderService) { }
  ngAfterViewInit(): void {
    this.initState();
  }


  getFullPrice(): number {
    return fullPrice(this.order);
  }

  getStatus(): string {
    return realStatus(this.order.orderStatus);
  }

  initState() {
    const selector = this.statusSelect.nativeElement;
    const children = selector.children;
    for (let i = 0 ; i < children.length ; i++ ) {
      if (children[i].value === this.order.orderStatus) {
        children[i].selected = true;
      }
    }
  }
  changeState(newStatus: OrderStatus) {
    console.log(this.order);
    this.orderService.updateOrder(new Order(this.order.orderItems, this.order.orderInfo,
      this.order.username, newStatus, this.order._id)).subscribe(data => console.log('Order updated'));
  }
}
