import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {OrderItem} from '../../entities/orderItem';
import {BasketService} from '../../services/basket.service/basket.service';


@Component({
  selector: 'app-basket-item',
  templateUrl: './basket-item.component.html',
  styleUrls: ['./basket-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BasketItemComponent implements OnInit {
  @Input() orderItem: OrderItem;
  constructor(private basketService: BasketService) { }

  ngOnInit() {
  }

  removeItem() {
    this.basketService.removeItem(this.orderItem.item);
  }
  addItem() {
    this.basketService.addItem(this.orderItem.item);
  }

}
