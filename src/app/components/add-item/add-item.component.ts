import { Component, OnInit } from '@angular/core';
import {ItemDataService} from '../../services/item-data.service/item-data.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Item} from '../../entities/item';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {
  form: FormGroup;
  addedItem: Item;
  items: Observable<Item[]>;
  constructor(private itemDataService: ItemDataService , private fb: FormBuilder) {
    this.buildForm();
  }

  ngOnInit() {
    this.items = this.itemDataService.getItems();
  }

  buildForm() {
    this.form = this.fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      'description': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(150)])],
      'price': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(8)])],
      'category': [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(25)])],
      'picture': [null, Validators.compose([Validators.required,
        Validators.pattern(new RegExp('[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)'))])]
    });
  }
  addItem(item: Item) {
    this.itemDataService.addItem(item).subscribe(recItem => this.addedItem = recItem);
  }

}
