import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Privilege, User} from '../../entities/user';
import {UserDataService} from '../../services/user-data.service/user-data.service';
import {State, StateService} from '../../services/stateService/state.service';
import {UserService} from '../../services/user.service/user.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {
  form: FormGroup;
  user: User;
  constructor(private fb: FormBuilder, private userService: UserService,
              private userDataService: UserDataService, private stateService: StateService) {
    this.buildForm();
  }

  ngOnInit() {
    this.userService.loggedUser.subscribe( user => {
      if (user && this.stateService.currentState === State.Register) {
        this.stateService.changeState(State.Browse);
      }
    });
  }

  buildForm() {
    this.form = this.fb.group({
      'username': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])]
    });
  }
  registerUser(user: User) {
    user.privilege = Privilege.NORMAL;
    this.userDataService.addUser(user).subscribe(registeredUser => {
      this.userService.login(registeredUser);
    });
  }

}
