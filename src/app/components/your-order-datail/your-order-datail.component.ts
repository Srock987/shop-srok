import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {fullPrice, Order, realStatus} from '../../entities/order';

@Component({
  selector: 'app-your-order-datail',
  templateUrl: './your-order-datail.component.html',
  styleUrls: ['./your-order-datail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class YourOrderDatailComponent implements OnInit {
  @Input() order: Order;
  constructor() { }

  ngOnInit() {
  }
  orderPrice() {
    return fullPrice(this.order);
  }
  orderState() {
    return realStatus(this.order.orderStatus);
  }

}
