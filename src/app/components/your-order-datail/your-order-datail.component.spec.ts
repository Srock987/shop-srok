import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourOrderDatailComponent } from './your-order-datail.component';

describe('YourOrderDatailComponent', () => {
  let component: YourOrderDatailComponent;
  let fixture: ComponentFixture<YourOrderDatailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourOrderDatailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourOrderDatailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
