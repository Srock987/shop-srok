import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Order, OrderStatus} from '../../entities/order';
import {OrderService} from '../../services/order.service/order.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import {from} from 'rxjs/observable/from';
import {OrderItem} from '../../entities/orderItem';

@Component({
  selector: 'app-manage-orders',
  templateUrl: './manage-orders.component.html',
  styleUrls: ['./manage-orders.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ManageOrdersComponent implements OnInit {
  orderTypes: any = OrderStatus;
  orderFilter: OrderStatus;
  filterSet: boolean;
  orders: Observable<Order[]>;
  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.getOrders();
  }
  getOrders() {
    this.orders = this.orderService.getOrders().mergeMap(orders => from(orders)).filter(order => {
      if (this.filterSet) {
        return order.orderStatus === this.orderFilter;
      } else {
        return true;
      }
    }).toArray();
    this.orderService.loadOrders();
  }
  setOrderFilter(status: OrderStatus) {
    this.orderFilter = status;
    this.filterSet = true;
    this.getOrders();
  }
  clearOrderFilter() {
    this.filterSet = false;
    this.getOrders();
  }

}
