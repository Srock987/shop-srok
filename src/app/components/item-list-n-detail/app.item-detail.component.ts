import {Component, Input} from '@angular/core';
import {Item} from '../../entities/item';
import {BasketService} from '../../services/basket.service/basket.service';

@Component({
  selector: 'app-item-detail',
  template: `
    <div class="itemDetail" *ngIf="item">
      <div class="row">
        <div class="col">
          <img class="img-fluid" id="picture" src="{{item.picture}}">
        </div>
        <div class="col">
          <h6>{{item.name}}</h6>
          <label>{{item.description}}</label>
        </div>
        <div class="col">
          <button (click)="addToBasket()">Add to basket</button>
        </div>
        <div class="col" style="align-items: center">
          <div class="priceTag">
            <label>{{item.price}}</label>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .itemDetail {
      border-radius: 25px;
      background: #73AD21;
      padding: 20px;
      margin: 15px;
    }
    .priceTag {
      border-radius: 10px;
      padding: 10px;
      background: #ccdfe2;
    }
    .itemDetail img {
      justify-content: center;
      border-radius: 5px;
    }
  `]
})

export class ItemDetailComponent {
  @Input() item: Item;
  constructor(private basketService: BasketService) {
  }
  addToBasket() {
    this.basketService.addItem(this.item);
  }
}
