import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {UserDataService} from '../../services/user-data.service/user-data.service';
import {User} from '../../entities/user';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WorkspaceComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }


}
