import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../entities/user';
import {State, StateService} from '../../services/stateService/state.service';
import {UserDataService} from '../../services/user-data.service/user-data.service';
import {UserService} from '../../services/user.service/user.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  form: FormGroup;
  user: User;
  constructor(private fb: FormBuilder, private stateService: StateService, private userService: UserService) {
    this.buildForm();
  }

  ngOnInit() {
    this.userService.loggedUser.subscribe( user => {
      if (user && this.stateService.currentState === State.Login) {
        this.stateService.changeState(State.Browse);
      }
    });
  }

  buildForm() {
    this.form = this.fb.group({
      'username': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15)])]
    });
  }

  login(providedUser: User) {
    this.userService.login(providedUser);
  }

  goToRegistration() {
    this.stateService.changeState(State.Register);
  }
}
