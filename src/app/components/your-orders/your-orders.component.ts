import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {OrderService} from '../../services/order.service/order.service';
import {Observable} from 'rxjs/Observable';
import {Order} from '../../entities/order';

@Component({
  selector: 'app-your-orders',
  templateUrl: './your-orders.component.html',
  styleUrls: ['./your-orders.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class YourOrdersComponent implements OnInit {
  userOrders: Observable<Order[]>;
  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.userOrders = this.orderService.loadUserOrders();
    this.userOrders.subscribe( orders => console.log(orders));
  }

}
