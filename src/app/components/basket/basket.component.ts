import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {BasketService} from '../../services/basket.service/basket.service';
import {Observable} from 'rxjs/Observable';
import {OrderItem} from '../../entities/orderItem';
import {State, StateService} from '../../services/stateService/state.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class BasketComponent implements OnInit {
  orderedItems: Observable<OrderItem[]>;
  constructor(private basketService: BasketService, private stateService: StateService) { }

  ngOnInit() {
    this.orderedItems = this.basketService.itemsArray;
  }
  continueShopping() {
    this.stateService.changeState(State.Browse);
  }
  placeOrder() {
    this.stateService.changeState(State.Order);
  }

}
