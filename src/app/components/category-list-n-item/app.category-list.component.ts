import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {ItemProviderService} from '../../services/item-provider.service/item-provider.service';
import {Observable} from 'rxjs/Observable';


@Component({
  selector: 'app-category-list',
  template: `
    <div class="categoryList">
      <div>
        <app-category-item [category]="'Home page'" (click)="setCategoryFilter('noFilter')"></app-category-item>
      </div>
      <div *ngFor="let category of categories | async">
        <app-category-item *ngIf="category" [category]="category"
                           (click)="setCategoryFilter(category)"></app-category-item>
      </div>
      <div>
        <label for="searchInput">Search for:</label><br>
        <input (change)="setNameFilter($event.target.value)" type="text" id="searchInput">
      </div>
      <div>
        <label for="slider">Price range:</label><br>
        <input (change)="setNameFilter($event.target.value)" type="range" id="slider">
      </div>
    </div>
  `,
  styles: [`
  .categoryList {
    padding: 20px;
    background: #13e259;
  }
  `]
})

export class CategoryListComponent implements AfterViewInit {

  @ViewChild('slider') slider: ElementRef;
  categories: Observable<string[]>;
  constructor(private itemProviderService: ItemProviderService) {
  this.categories = itemProviderService.getItemCategories();
  }

  ngAfterViewInit(): void {
    this.setUpSlider();
  }
  setUpSlider() {
    const rangeSlider = this.slider.nativeElement;
  }
  setCategoryFilter (filter: string) {
    this.itemProviderService.setCategoryFilter(filter);
  }
  setNameFilter(filter: string) {
    // this.itemProviderService.setNameFilter(filter);
  }
}
