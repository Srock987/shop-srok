import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-category-item',
  template: `
  <div class="categoryRow" *ngIf="category">
    <label>{{category}}</label>
  </div>
  `,
  styles: [`
  .categoryRow{
    border-radius: 15px;
    padding: 10px;
    margin: 15px;
    background: #e2bc2e;
  }
  `]
})

export class CategoryItemComponent {
  @Input() category: string;
}
