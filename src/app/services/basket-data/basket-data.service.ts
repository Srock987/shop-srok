import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SavedBasket} from '../../entities/savedBasket';
import {Observable} from 'rxjs/Observable';
import {IdHolder} from '../../entities/idHolder';

const saveBasketUrl = '/api/saveBasket';
const loadBasketUrl = '/api/loadBasket';

@Injectable()
export class BasketDataService {
  constructor(private http: HttpClient) { }
  saveBasket(save: SavedBasket) {
    this.http.post(saveBasketUrl, save).subscribe( data => console.log('Basket Saved'));
  }
  loadBasket(username: string): Observable<SavedBasket> {
    return this.http.post(loadBasketUrl, new IdHolder(username)).map(data => data as SavedBasket);
  }
}
