import { TestBed, inject } from '@angular/core/testing';

import { BasketDataService } from './basket-data.service';

describe('BasketDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BasketDataService]
    });
  });

  it('should be created', inject([BasketDataService], (service: BasketDataService) => {
    expect(service).toBeTruthy();
  }));
});
