import { Injectable } from '@angular/core';
import {BasketService} from '../basket.service/basket.service';
import {OrderInfo} from '../../entities/orderInfo';
import {Order} from '../../entities/order';
import {OrderDataService} from '../order-data.service/order-data.service';
import {Observable} from 'rxjs/Observable';
import {UserService} from '../user.service/user.service';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class OrderService {
  orders: BehaviorSubject<Order[]>;
  constructor(private basketService: BasketService, private orderDataService: OrderDataService, private userService: UserService) {
    this.orders = new BehaviorSubject([]);
  }
  placeOrder(userInfo: OrderInfo) {
    let order: Order;
    if (this.userService.currentLoadedUser) {
      order = new Order(this.basketService.currentItems, userInfo, this.userService.currentLoadedUser.username);
    } else {
      order = new Order(this.basketService.currentItems, userInfo);
    }
    return this.orderDataService.addOrder(order);
  }
  getOrders(): Observable<Order[]> {
    return this.orderDataService.getOrders();
  }
  loadOrders() {
    this.orderDataService.getOrders().subscribe( orders => this.orders.next(orders));
  }
  loadUserOrders() {
    const username = this.userService.currentLoadedUser.username;
    return this.orderDataService.getUserOrders(username);
  }
  updateOrder(order: Order) {
    return this.orderDataService.updateOrder(order);
  }

}
