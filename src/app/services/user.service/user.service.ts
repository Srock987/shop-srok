import { Injectable } from '@angular/core';
import {UserDataService} from '../user-data.service/user-data.service';
import {User} from '../../entities/user';
import 'rxjs/add/operator/map';
import {Token} from '../../entities/token';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {TokenValidation} from '../../entities/tokenValidation';

const CURRENT_TOKEN_KEY = 'currentTokenKey';
const CURRENT_TOKEN_ID = 'currentTokenId';

@Injectable()
export class UserService {
  private currentUser: User;
  private user: BehaviorSubject<User>;
  private tokenId: string;
  private tokenKey: string;

  constructor(private userDataService: UserDataService) {
    this.tokenKey = localStorage.getItem(CURRENT_TOKEN_KEY);
    this.tokenId = localStorage.getItem(CURRENT_TOKEN_ID);
    this.user = new BehaviorSubject(this.currentUser);
    this.autoLogin();
  }
  get loggedUser() {
    return this.user.asObservable();
  }
  get currentLoadedUser() {
    return this.currentUser;
  }
  getUserToken(providedUser: User) {
    return this.userDataService.loginUser(providedUser).map( token => {
      if (token) {
        this.setToken(token);
        return true;
      } else {
        return false;
      }
    });
  }
  setToken(token: Token) {
    this.tokenKey = token.key;
    this.tokenId = token._id;
    localStorage.setItem(CURRENT_TOKEN_KEY, token.key);
    localStorage.setItem(CURRENT_TOKEN_ID, token._id);
  }
  clearToken() {
    localStorage.removeItem(CURRENT_TOKEN_ID);
    localStorage.removeItem(CURRENT_TOKEN_KEY);
    this.tokenId = localStorage.getItem(CURRENT_TOKEN_ID);
    this.tokenKey = localStorage.getItem(CURRENT_TOKEN_KEY);
  }
  validateToken(): Observable<boolean> {
    return this.userDataService.validateToken(new Token(this.tokenId, this.tokenKey));
  }
  login(providedUser: User) {
    return this.getUserToken(providedUser).subscribe( response => {
      if (response) {
        console.log('User match');
         return this.validateToken().subscribe( result => {
          if (result) {
            console.log('Token validation successful');
            this.userDataService.getUser(this.tokenId).subscribe(user => {
              this.currentUser = user;
              this.user.next(this.currentUser);
            });
          } else {
            console.log('Token validation fail');
          }
        });
      } else {
        console.log('Bad username');
      }
    });
  }
  autoLogin() {
    this.validateToken().subscribe( result => {
      if (result) {
        this.userDataService.getUser(this.tokenId).subscribe(user => {
          console.log('Autologin succes');
          this.currentUser = user;
          this.user.next(this.currentUser);
        });
      } else {
        console.log('Autologin fail');
      }
    });
  }
  logout() {
    this.clearToken();
    this.currentUser = null;
    this.user.next(this.currentUser);
  }
}
