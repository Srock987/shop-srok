import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Order} from '../../entities/order';
import {IdHolder} from '../../entities/idHolder';


const addOrderUrl = '/api/addOrder';
const getOrdersUrl = '/api/getOrders';
const getUserOrdersUrl = '/api/userOrders';
const updateOrderUrl = '/api/updateOrder';

@Injectable()
export class OrderDataService {

  constructor(private http: HttpClient) { }

  addOrder(order: Order) {
    return this.http.post(addOrderUrl, order).map(data => data as Order);
  }
  getOrders() {
    return this.http.get(getOrdersUrl).map(data => data as Order[]);
  }
  getUserOrders(username: string) {
    return this.http.post(getUserOrdersUrl, new IdHolder(username)).map(data => data as Order[]);
  }
  updateOrder(order: Order) {
    return this.http.post(updateOrderUrl, order);
  }

}
