import { Injectable } from '@angular/core';
import {Item} from '../../entities/item';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {from} from 'rxjs/observable/from';
import 'rxjs/add/operator/reduce';
import {OrderItem} from '../../entities/orderItem';
import {BasketDataService} from '../basket-data/basket-data.service';
import {UserService} from '../user.service/user.service';
import {SavedBasket} from '../../entities/savedBasket';


@Injectable()
export class BasketService {
  private dataStore: {
    orderItems: OrderItem[]
  };
  private items: BehaviorSubject<OrderItem[]>;
  constructor(private basketDataService: BasketDataService, private userService: UserService) {
    this.dataStore = {
      orderItems: []};
    this.items = new BehaviorSubject(this.dataStore.orderItems);
    this.loadBasket();
    this.saveBaskets();
  }
  saveBaskets() {
    this.itemsArray.subscribe( ordItems => {
      if (this.userService.currentLoadedUser) {
        this.basketDataService.saveBasket(new SavedBasket(ordItems, this.userService.currentLoadedUser.username));
      }
    });
  }
  loadBasket() {
    this.userService.loggedUser.subscribe(user => {
      if (user && this.dataStore.orderItems.length === 0) {
        this.basketDataService.loadBasket(user.username).subscribe(savedBasket => {
          this.dataStore.orderItems = savedBasket.orderItems;
          this.items.next(this.dataStore.orderItems);
          console.log('Basket loaded');
        });
      } else if (!user ) {
        this.resetBasket();
        console.log('Basket lost');
      }
    });
  }

  get numberOfItems(){
    return this.items.flatMap(values => from(values)
      .map(item => item.quantity)
      .reduce((acc, val) => acc + val, 0));
  }
  get summaryPrice(){
    return this.items.flatMap(values => from(values)
      .reduce((acc, val) => acc + val.item.price * val.quantity, 0));
  }
  get itemsArray(){
    return this.items.asObservable();
  }
  get currentItems(){
    return this.dataStore.orderItems;
  }
  addItem(item: Item) {
    const index = this.dataStore.orderItems.findIndex((value) => value.item._id === item._id);
    console.log(index);
    if (index > -1) {
      this.dataStore.orderItems[index].quantity++;
    }else {
      this.dataStore.orderItems[this.dataStore.orderItems.length] = {item: item, quantity: 1};
    }
    this.items.next(this.dataStore.orderItems);
  }
  removeItem(item: Item) {
    const index = this.dataStore.orderItems.findIndex((value) => value.item._id ===  item._id);
    if (this.dataStore.orderItems[index].quantity > 1) {
      this.dataStore.orderItems[index].quantity--;
    }else {
      this.dataStore.orderItems.splice(index, 1);
    }
    this.items.next(this.dataStore.orderItems);
  }
  resetBasket() {
    this.dataStore.orderItems = [];
    this.items.next(this.dataStore.orderItems);
  }
}
