import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Item} from '../../entities/item';

const addItemUrl = '/api/addItem';
const getItemsUrl = '/api/getItems';

@Injectable()
export class ItemDataService {

  constructor(private http: HttpClient) { }
  addItem(item: Item) {
   return this.http.post(addItemUrl, item).map(data => data as Item);
  }
  getItems() {
    return this.http.get(getItemsUrl).map(data => data as Item[]);
  }

}
