import {Injectable} from '@angular/core';
import {Item} from '../../entities/item';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/filter';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toArray';
import 'rxjs/add/operator/mergeMap';
import {from} from 'rxjs/observable/from';
import {ItemDataService} from '../item-data.service/item-data.service';


const noFilter = 'noFilter';

@Injectable()
export class ItemProviderService {
  private filterPrice = {min: 0 , max: 200};
  private filterCategory: string = noFilter;
  private filterName: string = noFilter;
  private itemsObs: BehaviorSubject<Item[]>;
  private categoryObs: BehaviorSubject<string[]>;
  private dataStore: {
    items: Item[];
  };

  constructor(private itemDataService: ItemDataService) {
    this.dataStore = {items: []};
    this.itemsObs = <BehaviorSubject<Item[]>>new BehaviorSubject([]);
    this.categoryObs = <BehaviorSubject<string[]>>new BehaviorSubject([]);
    this.subscribeCategories();
  }
  subscribeCategories() {
    this.itemsObs.subscribe( items => {
      const categories: string[] = [];
      for (const item of this.dataStore.items) {
          if (categories.indexOf(item.category) === -1) {
            categories[categories.length] = item.category;
          }
        }
      this.categoryObs.next(categories);
      }
    );
  }

  setCategoryFilter(newFilter: string) {
    this.filterCategory = newFilter;
    this.loadItems();
  }

  setNameFilter(newFilter: string) {
    if (newFilter.length > 0) {
      this.filterName = newFilter;
    } else {
      this.filterName = noFilter;
    }
    this.loadItems();
  }
  setPriceFilter(min: number, max: number) {
    this.filterPrice.min = min;
    this.filterPrice.max = max;
    this.loadItems();
  }

  getItemCategories() {
    return this.categoryObs.asObservable();
  }

  loadItems() {
    this.itemDataService.getItems().subscribe(itemsData => {
      this.dataStore.items = itemsData;
      this.itemsObs.next(Object.assign({}, this.dataStore).items);
    });
  }

  get itemsObservable() {
    return this.itemsObs.asObservable()
      .mergeMap(items => from(items)
        .filter(item => this.filterCategory === noFilter || item.category === this.filterCategory)
        .filter(item => this.filterName === noFilter || !!this.filterName.match(item.name) )
        .filter(item => item.price >= this.filterPrice.min && item.price <= this.filterPrice.max )
        .toArray());
  }
}
