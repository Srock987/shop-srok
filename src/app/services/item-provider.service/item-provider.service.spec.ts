import { TestBed, inject } from '@angular/core/testing';

import { ItemProviderService } from './item-provider.service';

describe('ItemProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ItemProviderService]
    });
  });

  it('should be created', inject([ItemProviderService], (service: ItemProviderService) => {
    expect(service).toBeTruthy();
  }));
});
