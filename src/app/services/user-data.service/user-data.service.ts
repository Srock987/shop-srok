import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../../entities/user';
import {Token} from '../../entities/token';
import {IdHolder} from '../../entities/idHolder';

const getUserUrl = '/api/getUser';
const addUserUrl = '/api/addUser';
const loginUserUrl = '/api/loginUser';
const tokenAuthUrl = '/api/authToken';

@Injectable()
export class UserDataService {
  constructor(private http: HttpClient) { }
  getUser(userId: string) {
    return this.http.post(getUserUrl, new IdHolder(userId)).map(data => data as User);
  }
  loginUser(user: User) {
    return this.http.post(loginUserUrl, user).map(data => data as Token);
  }
  validateToken(token: Token): Observable<boolean> {
    return this.http.post(tokenAuthUrl, token).map( data => {
      return !!data;
    });
  }

  addUser(user: User) {
    return this.http.post(addUserUrl, user).map(data => data as User);
  }

}
