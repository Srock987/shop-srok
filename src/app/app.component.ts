import {Component, OnInit} from '@angular/core';
import {State, StateService} from './services/stateService/state.service';
import {Observable} from 'rxjs/Observable';
import {Privilege, User} from './entities/user';
import {UserService} from './services/user.service/user.service';

@Component({
  selector: 'app-root',
  template: `
    <div class="container-full" *ngIf="currentState | async; let state">
      <div class="topBasketBar">
        <div class="row">
          <div class="col-1">
            <button class="topButton" (click)="changeState(stateType.Browse)">Home</button>
          </div>
          <div class="col-1" *ngIf="!userLogged">
            <button class="topButton" (click)="changeState(stateType.Login)">Login</button>
          </div>
          <div class="col-1" *ngIf="userLogged">
            <button class="topButton" (click)="logout()">Logout</button>
          </div>
          <div class="col-3" *ngIf="userLogged">
            <div class="row" *ngIf="userAdmin">
              <div class="col">
                <button class="topButton" (click)="changeState(stateType.ManageItems)">Manage Items</button>
              </div>
              <div class="col">
                <button class="topButton" (click)="changeState(stateType.ManageOrders)">Manage Orders</button>
              </div>
            </div>
            <div class="row" *ngIf="!userAdmin">
              <div class="col-1">
                <button class="topButton" (click)="changeState(stateType.YourOrders)">Your Orders</button>
              </div>
            </div>
          </div>
          <div class="col">
            <app-top-basket></app-top-basket>
          </div>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Browse">
        <div class="col-3">
          <app-category-list></app-category-list>
        </div>
        <div class="col-9">
          <app-item-list></app-item-list>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Basket">
        <div class="col" align="center">
          <app-basket></app-basket>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Order">
        <div class="col" align="center">
          <app-order-placement></app-order-placement>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Login">
        <div class="col" align="center">
          <app-user-login></app-user-login>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Register">
        <div class="col" align="center">
          <app-user-register></app-user-register>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.Workspace">
        <div class="col" align="center">
          <app-workspace></app-workspace>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.ManageItems">
        <div class="col" align="center">
          <app-add-item></app-add-item>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.ManageOrders">
        <div class="col" align="center">
          <app-manage-orders></app-manage-orders>
        </div>
      </div>
      <div class="row" *ngIf="state === stateType.YourOrders">
        <div class="col" align="center">
          <app-your-orders></app-your-orders>
        </div>
      </div>
    </div>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  stateType: any = State;
  currentState: Observable<State>;
  currentUser: Observable<User>;
  userLogged: boolean;
  userAdmin: boolean;

  constructor(private stateService: StateService, private userService: UserService) {
  }

  ngOnInit(): void {
    this.currentState = this.stateService.getState;
    this.currentUser = this.userService.loggedUser;
    this.currentUser.subscribe( user => {
      if (user) {
        this.userLogged = true;
        this.userAdmin = user.privilege === Privilege.ADMIN;
      } else {
        this.userLogged = false;
      }
    });
  }
  changeState(state: State) {
    this.stateService.changeState(state);
  }
  logout() {
    this.userService.logout();
  }
}
